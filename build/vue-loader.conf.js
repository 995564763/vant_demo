'use strict'
const utils = require('./utils')               //引入项目的utils文件  主要是各种loader的生成
const config = require('../config')            //引入config文件，主要是dev以及prod环境的设置
const isProduction = process.env.NODE_ENV === 'production'  //判断当前是否为生产环境，如果是则返回true
const sourceMapEnabled = isProduction         //是否使用sourceMap，如果是生产环境就使用config文件中index.js中生产环境的配置，否则是否开发环境的配置
  ? config.build.productionSourceMap
  : config.dev.cssSourceMap

module.exports = {
  //utils文件cssLoaders返回的配置项，返回的格式为
  /* 
     loaders:{
        css:ExtractTextPlugin.extract({
        use: [cssLoader],
        fallback: 'vue-style-loader'
      }),
        postCss:{
         .......
      }
    }
    */
  loaders: utils.cssLoaders({
    sourceMap: sourceMapEnabled,
    extract: isProduction
  }),
  //是否使用sourceMap
  cssSourceMap: sourceMapEnabled,
  //是否使用cacheBusting，这个配置在config提到过
  cacheBusting: config.dev.cacheBusting,
  transformToRequire: {
    video: ['src', 'poster'],
    source: 'src',
    img: 'src',
    image: 'xlink:href'
  }
}

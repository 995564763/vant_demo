import util from '@/utils/util'

let SecretKey = 'D14EC4CD5BD00352FDE83C74DEBF2450'
let requestId = 'xbk-wechat-001'
let appId = 'wxddf2e96cf63c7f5d'         //测试 wxddf2e96cf63c7f5d 正式 wx0244f2d7dd34150c
let channelid = util.detect(navigator.userAgent, navigator.platform).weChat ? 'WXP30200203' : util.detect(navigator.userAgent, navigator.platform).ios ? 'IOS30200301' : 'AND30200301'
let acType = 'AC003', appCode = 'APP0005', IN_SOURCE = util.detect(navigator.userAgent, navigator.platform).weChat ? '03' : '02', merNo = util.detect(navigator.userAgent, navigator.platform).weChat ? '000002' : '000003', thirdType = '05'
let channel = util.detect(navigator.userAgent, navigator.platform).weChat ? 'wechat' : 'wxgcapp';
let domainName = 'ts.wxlyykt.com'
let shareName = 'ts.wxlyykt.com'

// 'www.wxlyykt.com'  //分享正式服
// 'ts.wxlyykt.com'  //分享测试服

//dev.wxlyykt.com  dev服
//api.wxlyykt.com  正式服
// merNo == 商户，
// appCode == 应用标识，02-默认微信公众号,04-app端
// thirdType == 第三方类型 1-微信 2-QQ 3-微博 4-小程序 5-微信公众号
//IN_SOURCE: '03', //03-默认微信公众号,02-app端
export { SecretKey, requestId, appId, channelid, channel, acType, appCode, IN_SOURCE, merNo, thirdType, domainName ,shareName }

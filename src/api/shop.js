/**
 * Mocking client-server processing
 */

import {shopCartList} from "../data/data.js"

export default {
  getProducts (cb) {
    setTimeout(() => cb(shopCartList), 100)
    console.log('shopCartList',shopCartList)
  },

  buyProducts (products, cb, errorCb) {
    setTimeout(() => {
      // simulate random checkout failure.
      (Math.random() > 0.5 || navigator.userAgent.indexOf('PhantomJS') > -1)
        ? cb()
        : errorCb()
    }, 100)
  }
}
import clone from 'lodash/clone'
import cloneDeep from 'lodash/cloneDeep'
import endsWith from 'lodash/endsWith'
import debounce from 'lodash/debounce'
import throttle from 'lodash/throttle'
import find from 'lodash/find'
import isEmpty from 'lodash/isEmpty'
import flatten from 'lodash/flatten'
import flattenDepth from 'lodash/flattenDepth'

export default {
  clone,
  cloneDeep,
  endsWith,
  debounce,
  throttle,
  find,
  isEmpty,
  flatten,
  flattenDepth
}

import avatar from '../assets/logo.png'

import Mock from 'mockjs'

const movies = []
// const shopCartList = []

Mock.Random.extend({
  movieclass:function () {
    let classes = ['xuanyi','xiognsha','aiqing','shabi']
    return this.pick(classes)
  }
})

for (let index = 0; index < 25; index++){
  movies.push(Mock.mock({
    moviename:Mock.Random.ctitle(4,15),
    movieclass:Mock.Random.movieclass(),
    'movieprice|1-100':2,
    movieintro:Mock.Random.cparagraph(),
    movieimage:Mock.Random.image('80x80','@color'),
    'moviesells|20-100':1,
    movieselling:Mock.Random.boolean()
  }))
}

let shopCartListMock = Mock.mock({
  'ListMock|3':[{
    shopName:Mock.Random.ctitle(4,15),
    'shopCartListData|1-10':[{
      productImage:Mock.Random.image('80x80','@color'),
      productName:Mock.Random.ctitle(4,15),
      productDes:Mock.Random.cparagraph(),
      productNum:Mock.Random.integer(1,2),
      productPricle:Mock.Random.integer(2,3),
      createdTime:Mock.mock('@datetime()'),
      checked:false,
      'id|+1': 1
    }],
    checked:false,
    'shopId|+1':1
  }]
})

const users =[
  {
    username: 'zsn12345',
    password: '123456',
    email: '123@163.com',
    tel: '18118900323',
    name: '李小白',
    time: '2017-11-11',
    avatar: avatar
  },
  {
    username: 'zhangsan',
    password: '123456',
    email: '321@163.com',
    tel: '13789546327',
    name: '张三',
    time: '2017-08-17',
    avatar: avatar
  }
];
const shopCartList = shopCartListMock.ListMock;

export {users,movies,shopCartList}

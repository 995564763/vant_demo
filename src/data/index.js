import axios from 'axios'
import Adapter from 'axios-mock-adapter'
import {users,movies} from "./data"
import avatarDefault from '../assets/logo.png'

export default {
  init(){
    // 创建Adapter 实例
    const mock = new Adapter(axios)

    //模拟登录接口
    mock.onPost('/login').reply(config => {
      console.log(config.data)
      // 解析axios传过来的数据
      let {username,password} = JSON.parse(config.data)
      return new Promise((res,rej)=>{
        // 先创建一个用户为空对象
        let user = {}
        // 判断模拟的假数据中是否有和传过来的数据匹配的
        let hasUser = users.some(person=>{
          // 如果存在这样的数据
          if(person.username === username && person.password === password){
            user = JSON.parse(JSON.stringify(person))
            user.password = ''
            return true
          }else{
            // 如果没有这个person
            return false
          }
        })
        if(hasUser){
          res([200,{code:200,msg:'登录成功',user}])
        }else{
          res([200,{code:500,msg:'账号或秘密错误'}])
        }
      })
    })
    // 模拟注册接口
    mock.onPost('/regin').reply(config => {
      let {username,password,email,tel,name} = config.params

      users.push({
        username: username,
        password: password,
        email: email,
        name: name,
        tel: tel,
        avatar: avatarDefault
      })

      return new Promise((res,rej)=>{
        res(config.data)
      })
    })
    //模拟获取商品列表
    // mock.onGet('/movielist').reply(config=>{
    //   let movieclass = config.params
    //   console.log(movieclass)
    //   let movielist = movies.filter(p=>{
    //     if(movieclass === ''){
    //       return true
    //     }else if(p.movieclass === movieclass){
    //       return true
    //     }else{
    //       return false
    //     }
    //   })
    //   return new Promise((res,rej)=>{
    //     res([200,{
    //       movielist
    //     }])
    //   })
    // })
  }
}

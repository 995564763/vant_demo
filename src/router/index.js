import Vue from 'vue'
import Router from 'vue-router'
import routeConfig from './routes'

Vue.use(Router)

export default new Router({
  // 配置路由
  routes: routeConfig
})

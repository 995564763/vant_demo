
const routes = [
  {
    path: '/',
    name: '首页',
    component: () => import('@/views/home'),
    redirect:'/movie',
    children:[
      {
        path:'/movie',
        name:'电影',
        icon:{
          normal: require('@/assets/img/movie.png'),
          active: require('@/assets/img/movie_active.png')
        },
        component:()=>import('@/views/pages/movies'),
        redirect:'/movie/list',
        children:[
          {
            // 这里用的动态路由，需要一个冒号：
            // path: '/movie/:class',
            path: '/movie/:class',
            components:{
              default:()=>import('@/views/pages/movie/movieList'),
              future: ()=>import('@/views/pages/movie/movieDetail')
            }
          }
        ]
      },
      {
        path:'/cinema',
        name:'影院',
        icon:{
          normal: require('@/assets/img/cinema.png'),
          active: require('@/assets/img/cinema_active.png')
        },
        component:()=>import('@/views/pages/cinema/cinema')
      },
      {
        path:'/shop',
        name:'购物',
        icon:{
          normal: 'https://img.yzcdn.cn/public_files/2017/10/13/c547715be149dd3faa817e4a948b40c4.png',
          active: 'https://img.yzcdn.cn/public_files/2017/10/13/793c77793db8641c4c325b7f25bf130d.png'
        },
        component:()=>import('@/views/pages/shop/shop')
      },
      {
        path:'/my',
        name:'我的',
        icon:{
          normal: require('@/assets/img/i.png'),
          active: require('@/assets/img/i_active.png')
        },
        component:()=>import('@/views/pages/my/my')
      }
    ]
  },
  {
    path:'/login',
    name:'账号登录',
    component:()=>import('@/views/login')
  },
  {
    path: '*',
    hidden: true,
    component: ()=>import('@/views/page404')
  }
]

export default routes

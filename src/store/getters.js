const getters = {
  footbar: state => state.app.footbar,
  // visitedViews: state => state.app.visitedViews,
  // token: state => state.user.token,
  // avatar: state => state.user.avatar,
  // name: state => state.user.name,
  // introduction: state => state.user.introduction,
  // status: state => state.user.status,
  // roles: state => state.user.roles,
  // setting: state => state.user.setting,
  // permission_routers: state => state.permission.routers,
  // addRouters: state => state.permission.addRouters,
  // id: state => state.user.id,
  // accountId: state => state.user.accountId
  getTodoById:(state)=>(id)=>{
    return state.todos.find(todo=>todo.id === id)
  }
}
export default getters

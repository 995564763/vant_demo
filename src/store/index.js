import Vue from 'vue'
import Vuex from 'vuex'
import app from './modules/app/app'
import user from './modules/user/index'
import cart from './modules/shop/cart'
import products from './modules/shop/products'
import movies from './modules/movies/movies'
// import permission from './modules/permission'
import getters from './getters'

Vue.use(Vuex)

const store = new Vuex.Store({
  modules:{
    app,
    user,
    cart,
    products,
    movies
  },
  getters
})

export default store

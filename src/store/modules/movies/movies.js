import {GetMovieList} from '@/api/api'

const state = {
  all:[]
}

const getters = {
  allMovies: state=> {
    let list =  state.all
    let result = list.map(item=>{
      item.img = item.img.replace('http:','https:').replace('/w.h/', '/128.0/')
      item.ver = item.ver.split('/')
      return item
    })
    console.log(result)

    return result 
  }
}

const mutations = {
  setMovies(state,movies){
    state.all = movies
  }
}

const actions = {
  getAllMovies({commit},args){
    return new Promise((resolve, reject) => {
      GetMovieList(args).then(res=>{
        console.log(res)
        commit('setMovies',res.data.result)
        resolve(res)
      }).catch(err => {
        console.log(err)
      })
    })
  }
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}

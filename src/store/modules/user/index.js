import state from './state'
import mutations from "./mutations"
import actions from "./actions"

const user = {
  state:state,
  mutations:mutations,
  actions:actions
}

export default user

import * as types from './mutation_types';

export default {
  [types.USER_LOGIN](state){
    state.user = JSON.parse(sessionStorage.getItem('user'))
  },
  [types.USER_LOGOUT](state){
    state.user = ''
  }
}

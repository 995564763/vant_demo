import shop from '@/api/shop'
const state = {
  added: [],
  checkoutStatus: null,
  allCheck:false
}

// getters
const getters = {
  checkoutStatus: state => state.checkoutStatus,

  cartProducts: (state, getters, rootState) => {
    return state.added.map(({ id, quantity }) => {
      let product;
      rootState.products.all.forEach(ele => {
        if (ele.shopCartListData.find(product => product.id === id)) {
            product = ele.shopCartListData.find(product => product.id === id);
        }
      });
      return {
        title: product.productName,
        price: product.productPricle,
        quantity:product.productNum
      }
    })
  },

  cartTotalPrice: (state, getters) => {
    return getters.cartProducts.reduce((total, product) => {
      return total + product.price * product.quantity
    }, 0)
  },
  
}
// mutations
const mutations = {
  pushProductToCart (state, { id }) {
    state.added.push({
      id,
      quantity: 1
    })
  },
  removeProductFromCart (state, {id}){
    const index = state.added.findIndex(added => added.id === id)
    state.added.splice(index, 1)
  },
  incrementItemQuantity (state, { id }) {
    const cartItem = state.added.find(item => item.id === id)
    cartItem.quantity++
  },
  setCartItems (state, { items }) {
    state.added = items
  },
  setCheckoutStatus (state, status) {
    state.checkoutStatus = status
  },
  allCheck: (state, { products }) => {
    let num = 0
    products.forEach( ele => {
      ele.checked ? num++ : '';
    })
    state.allCheck = num == products.length;
  },
  tiggerAllCheck: (state, { products }) => {
    
    products.forEach( ele => {
      ele.checked = !state.allCheck
    })
  },
}
// actions
const actions = {
  checkout ({ commit, state }, products) {
    const savedCartItems = [...state.added]
    commit('setCheckoutStatus', null)
    // empty cart
    commit('setCartItems', { items: [] })
    shop.buyProducts(
      products,
      () => commit('setCheckoutStatus', 'successful'),
      () => {
        commit('setCheckoutStatus', 'failed')
        // rollback to the cart saved before sending the request
        commit('setCartItems', { items: savedCartItems })
      }
    )
  },

  addProductToCart ({ state, commit,rootState}, product) {
    commit('setCheckoutStatus', null)
      const cartItem = state.added.find(item => item.id === product.id)
      if (!cartItem) {
        commit('pushProductToCart', { id: product.id })
      } else {
        commit('incrementItemQuantity', cartItem)
      }
      // remove 1 item from stock
      commit('products/decrementProductInventory', { id: product.id }, {root: true})
      commit('allCheck',{ products:rootState.products.all })
  },
  removeProductFromCart ({ state, commit,rootState}, product) {    
    const cartItem = state.added.find(item => item.id === product.id)
    if (cartItem) {
      commit('removeProductFromCart', { id: product.id })
    } else {
      console.log('_________________-')
    }
    commit('allCheck',{ products:rootState.products.all })
  },
  removeProduct ({ state, commit}, product) {  
    const cartItem = state.added.find(item => item.id === product.id)
    if (cartItem) {
      commit('removeProductFromCart', { id: product.id })
      commit('products/removeProduct', { id: product.id }, {root: true})
    } else {
      commit('products/removeProduct', { id: product.id }, {root: true})
    }
  },
  //设置全选
  tiggerAllCheck({ commit,rootState }){
    commit('tiggerAllCheck',{ products:rootState.products.all })
  }
}



export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}

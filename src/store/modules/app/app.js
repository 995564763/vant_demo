
const app = {
  state:{
    footbar:{
      opened:JSON.parse(sessionStorage.getItem('tabActive')) || 0
    },
    headbar:{
      title: JSON.parse(sessionStorage.getItem('headtitle'))||''
    }
  },
  getters:{
    getActive:state=>state.footbar.opened
  },
  mutations:{
    Change(state,active){
      sessionStorage.setItem('tabActive',active)
      state.footbar.opened = JSON.parse(sessionStorage.getItem('tabActive'))
    },
    GetTitle(state){
      state.headbar.title  = JSON.parse(sessionStorage.getItem('headtitle'))
    }
  },
  actions:{
    tabchange(context,active){
      console.log(active)
      context.commit('Change',active)
    },
    gettitle(context){
      context.commit('GetTitle')
    }
  }
}

export default app

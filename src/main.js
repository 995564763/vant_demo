// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
// import './commons/rem';
import Vue from 'vue'
import App from './App'
import router from './router'

import Vlf from 'vlf'
import Vuelidate from 'vuelidate'

import Vuex from 'vuex'
import store from './store/index'
import './errorLog' // error log
import './permission'  //权限
import Mock from './data/index'

//Mock.init()

//
import { currency } from '@/utils/currency'
Vue.filter('currency', currency)
//
Vue.use(Vuex)
Vue.use(Vuelidate)
Vue.use(Vlf)

Vue.config.productionTip = false

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  components: { App },
  template: '<App/>'
})
router.beforeEach((to,from,next)=>{
  if (to.path ==='/login'||to.path==='/regin'){
    sessionStorage.removeItem('user')
    store.dispatch('logout')
  }
  let user = sessionStorage.getItem('user')
  if (!user && (to.path === '/my')){
    next({path:'/login'})
  }else{
    next()
  }
});
